#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "thread.h"

#define M 3
#define K 2
#define N 3

#define THREADS 10

int A[M][K] = {{1, 4}, {2, 5}, {3, 6}};
int B[K][N] = {{8, 7, 6}, {5,4,3}};
int C[M][N];
int D[M][N] = {{28, 23, 18}, {41, 34, 27}, {54, 45, 36}};

typedef struct matrix{
    int i;
    int j;
}matrix;

void *multiply(void *args){
    matrix *data = args;
    int sum = 0;

    for(int k = 0; k < K; k++){
        sum += A[data->i][k] * B[k][data->j];
    }
    C[data->i][data->j] = sum;
    mythread_exit(0);
}

int main(int argc, char *argv[]){
    mythread_init();
    int count = 0;
    int flag = 1;
    printf("-----Testing matrix multiplication-----\n");
    printf("Matrix A is:\n");
     for(int l = 0; l < M; l++){
        for(int m = 0; m < K; m++){
            printf("%d ", A[l][m]);
        }
         printf("\n");   
    }

    printf("Matrix B is:\n");
     for(int l = 0; l < K; l++){
        for(int m = 0; m < N; m++){
            printf("%d ", B[l][m]);
        }
         printf("\n");   
    }      

    for(int i = 0; i < M; i++){
      for(int j = 0; j < N; j++){
        matrix * data = (matrix*)malloc(sizeof(matrix));
        data -> i = i;
        data -> j = j;
        mythread_t tid;
        mythread_create(&tid, NULL, multiply, data);
        mythread_join(tid, NULL);
        count++;
      }
    }  
    sleep(2);
    for(int i = 0; i < M; i++){
        for ( int j = 0; j < N; j++){
            if(C[i][j] != D[i][j]){
                flag = 0;
                break;
            }
        }
    }
    
    printf("The result of the matrix multiplication is:\n");
    for(int l = 0; l < M; l++){
        for(int m = 0; m < N; m++){
            printf("%d ", C[l][m]);
        }
         printf("\n");   
    }

    printf("Flag: %d\n", flag);
    for(int i = 0; i < M; i++){
        for ( int j = 0; j < N; j++){
            if(C[i][j] != D[i][j]){
                flag = 0;
                printf("%d, %d", C[i][j], D[i][j]);
                break;
            }
        }
    }
    if(flag == 1){
      
        printf("-----Test Passed-----\n");
    }
    else{
        printf("-----Test Failed-----\n");
    }

    return 0;
}
    

