#ifndef _MUTEX_H
#define _MUTEX_H

#define MUTEX_UNLOCKED (0u)
#define MUTEX_LOCKED (1u)


typedef struct mutex{
    int mutex_lock;
}mutex;

int mythread_mutex_init(mutex *mutex);
int mythread_mutex_lock(mutex *mutex);
int mythread_mutex_unlock(mutex *mutex);


#endif