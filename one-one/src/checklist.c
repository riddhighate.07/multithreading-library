#include <stdio.h>
#include <stdlib.h>
#include "thread.h"
#include "list.h"

int main(){
    list *test = (list*)malloc(sizeof(list));
    test -> head = NULL;
    test -> tail = NULL;
    
    mythread *thread = (mythread*)malloc(sizeof(mythread));
    mythread *thread1 = (mythread*)malloc(sizeof(mythread));
    thread -> tid = 1;
    insert_to_list(thread, test);
    
    thread1 -> tid = 2;
    insert_to_list(thread1, test);
    int x = isinlist(test, &thread1 -> tid);
    node  *display = test -> head;
    while(display != NULL){
        printf("%d\n", (display) -> list_thread -> tid);
        display = display -> next;
    }
    
    if(x == -1){
        printf("Not in list\n");
    }
    else{
        printf("Present in list at %d\n", x);
    }

    int z = islistempty(test);
    printf("z : %d", z);
    return 0;
}