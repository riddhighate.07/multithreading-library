#include <stdlib.h>
#include "list.h"
#include "thread.h"

void insert_to_list(mythread *thread, list *l){
    node *temp = (node*)malloc(sizeof(node));
    temp -> list_thread = thread;
    temp -> next = NULL;
    if(islistempty(l)){
        l -> head = l -> tail = temp;
    }
    else{
        temp -> prev = l -> tail;
        l -> tail -> next = temp;
        l -> tail = temp;
    }
    return;
}

mythread *remove_from_front(list *l){
    if(islistempty(l)){
        return NULL;
    }
    mythread *ret = (mythread*)malloc(sizeof(mythread));
    node *temp = (node*)malloc(sizeof(node*));
    temp = l -> head;
    ret = temp -> list_thread;
    l -> head = l -> head -> next;
    temp -> next = NULL;
    temp -> prev = NULL;

    if(l -> head == NULL){
        l -> tail = NULL;
    }
    else{
         l -> head -> prev = NULL;
    }

    return(ret);
    
}

int islistempty(list *l){
    return (!l->head && !l->tail);
}

int isinlist(list *l, mythread_t *thread){
    node *temp = (node*)malloc(sizeof(node));
    temp = l -> head;
    int pos = 0;
    while(((temp) -> list_thread -> tid != *thread) && temp -> next != NULL){
        pos++;
        temp = temp -> next;
    }
    
    if((temp) -> list_thread -> tid != *thread){
        return -1;
    }
    return (pos + 1);
}

mythread *get_from_list(list *l, mythread_t thread){
    node *temp = (node*)malloc(sizeof(node));
    temp = l -> head;
    int pos = 0;
    while(((temp) -> list_thread -> tid != thread) && temp -> next != NULL){
        pos++;
        temp = temp -> next;
    }
    
    if((temp) -> list_thread -> tid != thread){
        return NULL;
    }
    return temp->list_thread;
}

int task_count(list *l){
    node *temp = (node*)malloc(sizeof(node));
    temp = l -> head;
    int count = 0;
    while(temp != NULL){
        count++;
        temp = temp -> next;
    }
    return count;
}