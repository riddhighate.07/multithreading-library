#ifndef _SPINLOCK_H
#define _SPINLOCK_H

#include <stdio.h>


#define LOCKED (1u)
#define UNLOCKED (0u)

typedef struct spinlock{
     int status;
}spinlock;


int mythread_spin_init(spinlock *lock);
int mythread_spin_lock(spinlock *lock);
int mythread_spin_try_lock(spinlock *lock);
int mythread_spin_unlock(spinlock *lock);

#endif