#ifndef _SERVE_H
#define _SERVE_H

#include <signal.h>
#define PROTECTION_MODE (PROT_READ | PROT_WRITE)
#define MAP_PROTECTION (MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK)
void *stackalloc(size_t stack_size);
int stackdealloc(void *stack, size_t stacksize);

#endif