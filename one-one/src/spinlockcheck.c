#include <stdio.h>
#include "thread.h"
#include "spinlock.h"
#include <unistd.h>
long c = 0, c1 = 0, c2 = 0, run = 1;
int flag = 0;
spinlock lock;
void *thread1(void *arg) {
	while(run == 1) {
		mythread_spin_lock(&lock);
		c++;
		mythread_spin_unlock(&lock);
		c1++;
	}
	
}
void *thread2(void *arg) {
	
	while(run == 1) {
		mythread_spin_lock(&lock);
		c++;
		mythread_spin_unlock(&lock);
		c2++;
	}
}
int main() {
	mythread_init();
	mythread_t th1, th2;
	
	int t = mythread_spin_init(&lock);

	int x = mythread_create(&th1, NULL, thread1, NULL);
	int y = mythread_create(&th2, NULL, thread2, NULL);

	sleep(2);
	run = 0;
	mythread_join(th1, NULL);
	mythread_join(th2, NULL);
	printf("\n-----Testing race problem using spinlock in one-one model-----\n");
	fprintf(stdout, "c = %ld c1+c2 = %ld c1 = %ld c2 = %ld \n", c, c1+c2, c1, c2);
	if(c == c1 + c2){
		printf("-----Test passed-----\n");
	}
	else{
		printf("-----Test failed-----\n");
	}
	fflush(stdout);
}