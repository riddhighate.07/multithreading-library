#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <syscall.h>
#include <stdatomic.h>
#include <asm/prctl.h>
#include <sys/prctl.h>
#include <linux/futex.h>
#include <stdint.h>
#include <sys/time.h>
#include <unistd.h>
#include "thread.h"
#include "mutex.h"

int atomic_compare_and_swapp(int *mutex_status, int expected, int desired){

   return atomic_compare_exchange_strong(mutex_status, &expected, desired);
}


int mythread_mutex_init(mutex *mutex_ptr){
    if(!mutex_ptr){
        errno = EINVAL;
        return errno;
    }
    mutex_ptr = (mutex*)malloc(sizeof(mutex));
    if(!mutex_ptr){
        errno = ENOMEM;
        return errno;
    }
    mutex_ptr -> mutex_lock = MUTEX_UNLOCKED;
    return 0;

}

int mythread_mutex_lock(mutex *mutex_ptr){
    mythread *thread;
    thread = get_current_thread();
    while(!atomic_compare_and_swapp(&mutex_ptr -> mutex_lock, MUTEX_UNLOCKED, MUTEX_LOCKED)){
        thread -> state = THREAD_BLOCKED;
        syscall(SYS_futex,&mutex_ptr -> mutex_lock, FUTEX_WAIT, MUTEX_LOCKED, NULL, NULL, 0);
        thread -> state = THREAD_RUNNING;
    }
    return 0;
}

int mythread_mutex_unlock(mutex *mutex_ptr){
    atomic_compare_and_swapp(&mutex_ptr -> mutex_lock, MUTEX_LOCKED, MUTEX_UNLOCKED);
    syscall(SYS_futex,&mutex_ptr -> mutex_lock, FUTEX_WAKE, 1, NULL, NULL, 0);
    return 0;
}
