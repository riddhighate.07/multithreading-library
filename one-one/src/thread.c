#define _GNU_SOURCE
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <sched.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <asm/prctl.h>
#include <sys/prctl.h>
#include <linux/futex.h>
#include "thread.h"
#include "serve.h"
#include "spinlock.h"
#include "list.h"

list *task_queue;
spinlock *lock;

void cleanup(){
    int n = task_count(task_queue);
    mythread *thread = (mythread*)malloc(sizeof(mythread));
    
    while(n > 0){
        thread = remove_from_front(task_queue);
        stackdealloc(thread -> stack, thread -> stack_size);
        n--;
    }
    free(thread);
    free(task_queue);
}

//Function to get the current thread
mythread* get_current_thread(void){
    long address;
    syscall(SYS_arch_prctl, ARCH_GET_FS, &address);
    return (mythread*)address;
}
//Function to launch the start routine
int function_start(void *thread){
    mythread *t = (mythread*)thread;
    t -> return_value = t -> thread_start(t -> args);
    t -> state = THREAD_EXITED;
    return 0;
}

//First function to be called in the main application program. This creates the main thread.
int mythread_init(void){
    mythread *thread = (mythread*)calloc(1, sizeof(mythread));
    atexit(cleanup);
    thread -> args = NULL;
    thread -> return_value = NULL;
    thread -> thread_start = NULL;
    thread -> stack = NULL;
    thread -> stack_size = 0;
    thread -> tid = syscall(SYS_gettid);
    thread -> futex = 0;
    
    task_queue = (list*)malloc(sizeof(list));
    mythread_spin_init(lock);
    return 0; 
}

//Function for creating the thread
int mythread_create(mythread_t *thread, mythread_attr *attr, void *(*thread_start)(void *), void *args){
    
    if(!thread || !thread_start){
        errno = EINVAL;
       
        return errno;
    }
          
    
    mythread *new_thread = (mythread*)calloc(1, sizeof(mythread));
    if(!new_thread){
        errno = ENOMEM;
       
        return errno;
    }

    new_thread -> state = THREAD_RUNNING; 
    new_thread -> thread_start = thread_start;                 
    new_thread -> args = args; 
    new_thread -> stack_size = STACK_SIZE;
    new_thread -> stack = stackalloc(new_thread -> stack_size);
    

    new_thread -> tid = clone( &function_start, new_thread -> stack + new_thread -> stack_size, CLONE_VM | CLONE_SIGHAND 
    | CLONE_FS | CLONE_FILES | CLONE_THREAD | CLONE_SYSVSEM | CLONE_SETTLS | CLONE_CHILD_CLEARTID | CLONE_PARENT_SETTID, new_thread, new_thread -> futex, new_thread, new_thread -> futex);   

    if(new_thread -> tid == EINVAL || new_thread -> tid == ENOMEM){
        printf("Stack problem");
    
    }

    *thread = new_thread -> tid;

    if(new_thread -> tid < 0){
        printf("Clone failed\n");
        errno = EINVAL;
        return errno;
    } 
    insert_to_list(new_thread, task_queue);

    return 0;
}

//Function to join the thread
int mythread_join(mythread_t thread, void **return_value){

    mythread *thread_join = (mythread*)malloc(sizeof(mythread));
    thread_join = get_from_list(task_queue, thread);

    if(thread_join -> state == THREAD_JOINED){
        errno = EINVAL;
        return errno;
    }    

    if(isinlist(task_queue, &thread) <= 0){
        errno = ESRCH;
        return errno;
    }



    if(thread_join -> futex){
        thread_join -> state = THREAD_WAIT_JOINING;
        syscall(SYS_futex, thread_join -> futex, FUTEX_WAIT, thread_join -> tid, NULL, NULL, 0);
    }

    thread_join -> state = THREAD_JOINED;
    if(return_value){
        *return_value = thread_join -> return_value;
    }
    return 0;
}

//Function to exit the thread
void mythread_exit(void *ret){
    mythread *current_thread = get_current_thread();
    sleep(2);
    if(current_thread == NULL){
        printf("Current thread is NULL\n");
    }
    current_thread -> return_value = ret;
    current_thread -> state = THREAD_EXITED;
    syscall(SYS_exit, 0);
}

//Function to kill the thread
int mythread_kill(mythread_t thread, int sig){
    
    mythread *thread_kill = (mythread*)malloc(sizeof(mythread));
    thread_kill = get_from_list(task_queue, thread);
    if(thread_kill -> state == THREAD_EXITED || thread_kill -> state == THREAD_JOINED){
        errno == EINVAL;
        return errno;
    }
    int kid = getpid();
    syscall(SYS_tgkill, kid, thread_kill -> tid, sig);
    return 0;
}