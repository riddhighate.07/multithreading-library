#ifndef _LIST_H
#define _LIST_H

#include <stdio.h>
#include "thread.h"

typedef struct node{
    mythread *list_thread;
    struct node *prev;
    struct node *next;
}node;

typedef struct list
{
    struct node *head;
    struct node *tail;
}list;


void insert_to_list(mythread *thread, list *l);
mythread *remove_from_front(list *l);
int islistempty(list *l);
int isinlist(list *l, mythread_t *thread);
mythread *get_from_list(list *l, mythread_t thread);
int task_count(list *l);

#endif 