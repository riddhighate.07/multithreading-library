#include <stdio.h>
#include <unistd.h>
#include "thread.h"
#include "serve.h"

typedef struct {
    int *ar;
    long n;
} subarray;


void *
incer(void *arg)
{
    long i;


    for (i = 0; i < ((subarray *)arg)->n; i++)
        ((subarray *)arg)->ar[i]++;
}


int main(void)
{
    mythread_init();
    int        ar[1000000];
    mythread_t  th1, th2;
    subarray   sb1, sb2;


    sb1.ar = &ar[0];
    sb1.n  = 500000;
    (void) mythread_create(&th1, NULL, incer, &sb1);


    sb2.ar = &ar[500000];
    sb2.n  = 500000;
    (void) mythread_create(&th2, NULL, incer, &sb2);


    int y = mythread_join(th1, NULL);
    int z = mythread_join(th2, NULL);
    printf("\n------Joining the thread in one-one model------\n");
    if(y == 0 && z == 0){
        printf("-----Test passed-----\n");
    }
    else{
        printf("-----Test failed %d-----\n", y);
    }

    y = mythread_join(th1, NULL);
    printf("\n-----Joining the thread in one-one model which is already joined-----\n");
        if(y == 22){
            printf("Thread returned EINVAL error number\n");
            printf("-----Test passed-----\n");
        }
        else{
            printf("-----Test Failed-----\n");
        }

    
    
    return 0;
}