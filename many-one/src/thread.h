#ifndef _THREAD_H
#define _THREAD_H

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include "serve.h"

#define STACK_SIZE 1024*64
//64KB

#define MAX_THREADS 50

typedef pid_t mythread_t;

enum {
    THREAD_RUNNING,
    THREAD_READY,
    THREAD_EXITED,
    THREAD_DETACHED,
    THREAD_JOINED,
    THREAD_JOINABLE,
    THREAD_WAIT_JOINING,
    THREAD_BLOCKED
};

typedef struct mythread{
    mythread_t tid;
    void *stack;
    size_t stack_size;
    void *return_value;
    struct mythread *next;
    void *(*thread_start)(void *);
    int state;
    void *args;
    int futex;
    ucontext_t *thread_context;
    mythread_t joined_thread_id;
    sigset_t sig;
}mythread;

typedef struct mythread_attr{
    int temp;
}mythread_attr;

int mythread_init(void);
int mythread_create(mythread_t *thread, mythread_attr *attr , void *(*thread_start)(void *), void *args);
int mythread_join(mythread_t thread, void **return_value);
void mythread_exit(void *ret);
int mythread_kill(mythread_t thread, int sig);
mythread *get_current_thread(void);

#endif