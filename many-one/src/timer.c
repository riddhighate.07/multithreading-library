#include <sys/time.h>
#include "timer.h"

void timer_start(struct itimerval *timer){
    timer -> it_interval.tv_sec = 0;
    timer -> it_interval.tv_usec = MICROSECONDS;
    timer -> it_value.tv_sec = 0;
    timer -> it_value.tv_usec = MICROSECONDS;
    setitimer(ITIMER_REAL, timer, 0);
}

void timer_stop(struct itimerval *timer){
    timer -> it_interval.tv_sec = 0;
    timer -> it_interval.tv_usec = 0;
    timer -> it_value.tv_sec = 0;
    timer -> it_value.tv_usec = 0;
    setitimer(ITIMER_REAL, timer, 0);
}

void enable_interrupt(struct itimerval *timer){
    timer_start(timer);
}
void disable_interrupt(struct itimerval *timer){
    timer_stop(timer);
}
