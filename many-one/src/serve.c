#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/mman.h>
#include "serve.h"

void *stackalloc(size_t stacksize){
        size_t pagesize = sysconf(_SC_PAGESIZE); 
        void *stack = mmap(NULL, stacksize + pagesize, PROTECTION_MODE, MAP_PROTECTION, -1, 0);

        if(stack == MAP_FAILED){
            return NULL;
        }

        if(mprotect(stack, pagesize, PROT_NONE) == -1){
            printf("mprotect failed\n");
            munmap(stack, stacksize + pagesize);
            return NULL;
        }
        return stack + pagesize;

}

int stackdealloc(void *stack, size_t stacksize){
    size_t pagesize = sysconf(_SC_PAGESIZE);
    return munmap(stack - pagesize, stacksize + pagesize);
}