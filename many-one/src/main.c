#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include "thread.h"
#include "serve.h"

int testhandler = 0;
void *fun(void *arg){
    int *temp = (int*)arg;
    printf("Function Value: %d\n", *temp);
    return (void*)temp;
}

void sighandler(int signo){
    printf("Thread in signal handler\n");
    testhandler = 0;
}

void *killfun(void *arg){
    while(testhandler);
    return (void *)128;
}

int main(){
    mythread_init();
    mythread_t pthread;
    printf("------Creating a thread in many-one model------\n");
    int i = 42;
    int *temp = &i;
    int x = mythread_create(&pthread, NULL, fun, (void*)temp);
    sleep(1);
    
    if(x == 0){
        printf("-----Test Passed-----\n");
    }
    else{
        printf("-----Test failed-----\n");
    }

    printf("\n------Creating a thread in many-one model with null function------\n");
    x = mythread_create(&pthread, NULL, NULL, (void*)temp);
    
    if(x == 22){
        printf("mythread_create() returned EINVAL\n");
        printf("-----Test Passed-----\n");
    }
    else{
        printf("-----Test failed-----\n");
    }

    printf("\n------Creating a thread in many-one model with null pointer to mythread_t------\n");
    x = mythread_create(NULL, NULL, fun, (void*)temp);
    
    if(x == 22){
        printf("mythread_create() returned EINVAL\n");
        printf("-----Test Passed-----\n");
    }
    else{
        printf("-----Test failed-----\n");
    }
    
     return 0;
}