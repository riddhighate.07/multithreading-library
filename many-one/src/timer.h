#ifndef _TIMER_H
#define _TIMER_H

#define MICROSECONDS 2000
#include <sys/time.h>

void timer_start(struct itimerval *timer);
void timer_stop(struct itimerval *timer);
void enable_interrupt(struct itimerval *timer);
void disable_interrupt(struct itimerval *timer);


#endif