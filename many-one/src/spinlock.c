#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <syscall.h>
#include <stdatomic.h>
#include <asm/prctl.h>
#include <sys/prctl.h>
#include <unistd.h>
#include "spinlock.h"
#include "thread.h"

int atomic_compare_and_swap(int *spin_status, int expected, int desired){

   return atomic_compare_exchange_strong(spin_status, &expected, desired);
}

int mythread_spin_init(spinlock *lock){
    if(!lock){
        errno = EINVAL;
        return errno;
    }    

    lock = (spinlock*)malloc(sizeof(spinlock));
    if(!lock){
        errno = ENOMEM;
        return errno;
    }

    lock -> status = UNLOCKED;
    return 0;
}

int mythread_spin_lock(spinlock *lock){
    if(!lock){
        errno = EINVAL;
        return errno;
    }
    while(!atomic_compare_and_swap(&lock -> status, UNLOCKED, LOCKED));
    
    
    return 0;
}

int mythread_spin_try_lock(spinlock *lock){
    if(!lock){
        errno = EINVAL;
        return errno;
    }

    if(!atomic_compare_and_swap(&lock -> status, UNLOCKED, LOCKED)){
        errno = EBUSY;
        return errno;
    }

    return 0;

}

int mythread_spin_unlock(spinlock *lock){
    if(!lock){
        errno = EINVAL;
        return errno;
    }
    atomic_compare_and_swap(&lock -> status, LOCKED, UNLOCKED);
    return 0;

}