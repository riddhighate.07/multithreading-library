//Credits: DanGe42's gist on context_demo was referred for writing the scheduler code

#define _GNU_SOURCE
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sched.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <asm/prctl.h>
#include <sys/prctl.h>
#include <linux/futex.h>
#include <ucontext.h>
#include "thread.h"
#include "serve.h"
#include "spinlock.h"
#include "list.h"
#include "timer.h"

int current_thread_id = 0;
list *ready_queue;
static struct itimerval timer;
mythread *schedule_thread;
ucontext_t *ucp;

//Function to launch the start routine
static void function_start(void){
    mythread *thread;
    thread = get_current_thread();
    thread -> return_value = thread -> thread_start(thread -> args);
    thread -> state = THREAD_EXITED;
    ucontext_t *current = (ucontext_t*)malloc(sizeof(ucontext_t));
    current -> uc_link = 0;
    current -> uc_flags = 0;
    current -> uc_stack.ss_size = STACK_SIZE;
    current -> uc_stack.ss_sp = stackalloc(STACK_SIZE);
    getcontext(current);
    swapcontext(current, ucp);
}

static inline void *get_fs(void) {
    long address;
    syscall(SYS_arch_prctl, ARCH_GET_FS, &address);
    return (void *)address;
}

void cleanup(){
    int n = task_count(ready_queue);
    mythread *thread = (mythread*)malloc(sizeof(mythread));
    
    while(n > 0){
        thread = remove_from_front(ready_queue);
        stackdealloc(thread -> stack, thread -> stack_size);
        n--;
    }
    free(thread);
    free(ready_queue);
}

void scheduler(){
    disable_interrupt(&timer);
    //schedule_thread = (mythread*)malloc(sizeof(mythread));
    int count = task_count(ready_queue);
    void *old;
    old = get_fs();
    while(count > 0){
        schedule_thread = remove_from_front(ready_queue);
        
        if(schedule_thread -> state == THREAD_READY){
            syscall(SYS_arch_prctl, ARCH_SET_FS, schedule_thread);
            timer_start(&timer);
            ucp = (ucontext_t*)malloc(sizeof(ucontext_t));
            ucp -> uc_link = 0;
            ucp -> uc_flags = 0;
            ucp -> uc_stack.ss_size = STACK_SIZE;
            ucp -> uc_stack.ss_sp = stackalloc(STACK_SIZE);
            for(int i = 1; i < NSIG; i++){
                if(sigismember(&schedule_thread->sig, i)){
                    raise(i);
                    sigdelset(&schedule_thread->sig, i);
                }
            }
            getcontext(ucp);
            schedule_thread -> thread_context -> uc_link = ucp;
            swapcontext(ucp, schedule_thread->thread_context);
            timer_stop(&timer);
            
            syscall(SYS_arch_prctl, ARCH_SET_FS, old);
              switch (schedule_thread -> state){
                    case THREAD_EXITED:
                        insert_to_list(schedule_thread, ready_queue);
                        break;
                    
                    default:
                        break;
                    }
    
        }
        else if(schedule_thread -> state = THREAD_EXITED){
            timer_start(&timer);
            insert_to_list(schedule_thread, ready_queue);
            timer_stop(&timer);
        }
        else{
            return;
        }
        count--;
    }
  
    
    enable_interrupt(&timer);
}

void set_signals(void){
    struct sigaction action;
    sigset_t mask;
    sigfillset(&mask);
    action.sa_handler = scheduler;
    action.sa_mask = mask;
    action.sa_flags = 0;

    if(sigaction(SIGALRM, &action, NULL) != 0){
        perror("Bad signal handler\n");
    }
}

//Function to get the current thread
mythread* get_current_thread(void){
    long address;
    syscall(SYS_arch_prctl, ARCH_GET_FS, &address);
    return (mythread*)address;
}

//First function to be called in the main application program. This creates the main thread.
int mythread_init(void){
    schedule_thread = (mythread*)calloc(1, sizeof(mythread));
    schedule_thread -> args = NULL;
    schedule_thread -> return_value = NULL;
    schedule_thread -> thread_start = NULL;
    schedule_thread -> stack = NULL;
    schedule_thread -> stack_size = 0;
    schedule_thread -> tid = current_thread_id++;
    schedule_thread -> futex = 0;
    schedule_thread -> thread_context = (ucontext_t*)malloc(sizeof(ucontext_t));
    schedule_thread -> joined_thread_id = -1;
    atexit(cleanup);
    getcontext(schedule_thread -> thread_context);
    ready_queue = (list*)malloc(sizeof(list));
    ready_queue -> head = ready_queue -> tail = NULL;
    set_signals();
    timer_start(&timer);
    return 0; 
}

//Function for creating the thread
int mythread_create(mythread_t *thread, mythread_attr *attr, void *(*thread_start)(void *), void *args){
    
    if(!thread || !thread_start){
        errno = EINVAL;
        return errno;
    }
          
    
    mythread *new_thread = (mythread*)calloc(1, sizeof(mythread));
    if(!new_thread){
        errno = ENOMEM;
        return errno;
    }
    
    disable_interrupt(&timer);

    new_thread -> tid = current_thread_id++;
    new_thread -> state = THREAD_RUNNING; 
    new_thread -> thread_start = thread_start;                 
    new_thread -> args = args; 
    new_thread -> futex = 1;
    new_thread -> joined_thread_id = -1;
    new_thread -> stack_size = STACK_SIZE;
    new_thread -> stack = stackalloc(new_thread -> stack_size);
    new_thread -> thread_context = (ucontext_t*)malloc(sizeof(ucontext_t));
    new_thread -> thread_context -> uc_link = 0;
    new_thread -> thread_context -> uc_stack.ss_sp = stackalloc(new_thread -> stack_size);
    new_thread -> thread_context -> uc_stack.ss_size = new_thread -> stack_size;
    new_thread -> thread_context -> uc_flags = 0;
    

    getcontext(new_thread -> thread_context);
    makecontext(new_thread -> thread_context, function_start, 0);


    if(new_thread -> tid == EINVAL || new_thread -> tid == ENOMEM){
        printf("Stack problem");
    }

    new_thread -> state = THREAD_READY;
    insert_to_list(new_thread, ready_queue);
    
    *thread = new_thread -> tid;
    
    enable_interrupt(&timer);
    return 0;
}

//Function to join the thread
int mythread_join(mythread_t thread, void **return_value){
    disable_interrupt(&timer);
    
    mythread *thread_join = (mythread*)malloc(sizeof(mythread));
    thread_join = get_from_list(ready_queue, thread);

    if(thread_join -> joined_thread_id != -1){
        errno = EINVAL;
        return errno;
    }

    if(isinlist(ready_queue, &thread) <= 0){
        errno = ESRCH;
        return errno;
    }

    thread_join -> joined_thread_id = current_thread_id;
    schedule_thread -> state = THREAD_WAIT_JOINING;

    printf("Current thread id: %d\n", schedule_thread -> tid);
    if(thread_join -> state != THREAD_EXITED){
        swapcontext(thread_join -> thread_context, schedule_thread -> thread_context);
    }
    schedule_thread -> state = THREAD_RUNNING;
    thread_join -> state = THREAD_JOINED;
    enable_interrupt(&timer);
    if(return_value){
        *return_value = thread_join -> return_value;
    }
    return 0;
}

//Function to exit the thread
void mythread_exit(void *ret){
    disable_interrupt(&timer);
    mythread *current_thread = get_current_thread();
    if(current_thread == NULL){
        printf("Current thread is NULL\n");
    }
    current_thread -> return_value = ret;
    current_thread -> state = THREAD_EXITED;
    setcontext(ucp);
    enable_interrupt(&timer);
}

//Function to kill the thread
int mythread_kill(mythread_t thread, int sig){
    
   mythread *thread_kill = (mythread*)malloc(sizeof(mythread));
    thread_kill = get_from_list(ready_queue, thread);
    if(thread_kill -> state == THREAD_EXITED || thread_kill -> state == THREAD_JOINED){
        errno == EINVAL;
        return errno;
    }
    int kid = getpid();
    syscall(SYS_tgkill, kid, thread_kill -> tid, sig);
    return 0;
}