SUBDIRS = one-one many-one

.PHONY: all clean

run: 
	$(MAKE) -C one-one
	$(MAKE) -C many-one

all clean:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir -f Makefile $@; \
	done